package ru.malakhov.tm.command.project;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeOneById(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}