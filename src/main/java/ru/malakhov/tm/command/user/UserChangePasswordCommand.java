package ru.malakhov.tm.command.user;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.TerminalUtil;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.CHANGE_PASSWORD;
    }

    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER CURRENT PASSWORD:");
        final String currentPassword = TerminalUtil.nextLine();
        if (!HashUtil.salt(currentPassword).equals(serviceLocator.getUserService().findById(userId).getPasswordHash())) {
            throw new EmptyPasswordException();
        }
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().changePassword(userId, newPassword);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
