package ru.malakhov.tm.command.user;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;

public final class UserProfileCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROFILE;
    }

    @Override
    public String description() {
        return "Profile.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROFILE]");
        String[] profile = serviceLocator.getUserService().profile(userId);
        for (int i = 0; i < profile.length; i++) {
            System.out.println(profile[i]);
        }
    }

}