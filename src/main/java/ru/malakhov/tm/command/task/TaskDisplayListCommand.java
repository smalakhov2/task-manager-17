package ru.malakhov.tm.command.task;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public final class TaskDisplayListCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_LIST;
    }

    @Override
    public String description() {
        return "Display task list.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}