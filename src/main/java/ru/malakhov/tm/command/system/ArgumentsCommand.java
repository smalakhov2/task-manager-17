package ru.malakhov.tm.command.system;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;

import java.util.List;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    public String argument() {
        return ArgumentConst.ARGUMENTS;
    }

    @Override
    public String name() {
        return TerminalConst.ARGUMENTS;
    }

    @Override
    public String description() {
        return "Display arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final AbstractCommand command: commands) {
            if (command.argument() == null) continue;
            System.out.println(command.argument() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}