package ru.malakhov.tm.exception.system;

import ru.malakhov.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String command) {
        super("Error! Unknown ``" + command + "`` argument...");
    }

}