package ru.malakhov.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String ABOUT = "about";

    String VERSION = "version";

    String INFO = "info";

    String EXIT = "exit";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    String TASK_LIST = "task-list";

    String TASK_CLEAR = "task-clear";

    String TASK_CREATE = "task-create";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_DISPLAY_BY_ID = "task-display-by-id";

    String TASK_DISPLAY_BY_NAME = "task-display-by-name";

    String TASK_DISPLAY_BY_INDEX = "task-display-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String PROJECT_LIST = "project-list";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_CREATE = "project-create";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_DISPLAY_BY_ID = "project-display-by-id";

    String PROJECT_DISPLAY_BY_NAME = "project-display-by-name";

    String PROJECT_DISPLAY_BY_INDEX = "project-display-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String LOGIN = "login";

    String LOGOUT = "logout";

    String REGISTRY = "registry";

    String PROFILE = "profile";

    String CHANGE_EMAIL = "change-email";

    String CHANGE_LOGIN = "change-login";

    String CHANGE_PASSWORD = "change-password";

    String CHANGE_FIRST_NAME = "change-first-name";

    String CHANGE_LAST_NAME = "change-last-name";

    String CHANGE_MIDDLE_NAME = "change-middle-name";

    String LOCK_USER = "lock user";

    String UNLOCK_USER = "unlock user";

    String REMOVE_USER_BY_ID = "remove-user-by-id";

    String REMOVE_USER_BY_LOGIN = "remove-user-by-login";

}