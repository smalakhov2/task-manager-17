package ru.malakhov.tm.constant;

public interface PathConst {

    String BINARY_PATH = "./data_binary.bin";

    String BASE64_PATH = "./data_base64.base64";

}
